# -*- coding: utf-8 -*-
"""
Created on Fri Jan 18 11:39:40 2019

@author: mille
"""

from PyQt5 import QtGui, uic

ui_form_curve, ui_base_curve = uic.loadUiType(r'tagazouPlot_CurveWidget.ui')

class curveWidget(ui_base_curve, ui_form_curve):

    def __init__(self, parent=None, number=1):
        super(ui_base_curve, self).__init__(parent)
        self.setupUi(self)
        self.label_number.setText("Curve n° "+str(number))

        # LineEdit for xmin and xmax
        # DoubleValidator oblige user
        # to put numbers into those lineEdit
        val = QtGui.QDoubleValidator()
        self.lineEdit_xmin.setValidator(val)
        self.lineEdit_xmax.setValidator(val)

if __name__ == '__main__':
    curvewid = curveWidget()
    curvewid.show()

