# -*- coding: utf-8 -*-
"""
Created on Mon Feb 11 10:22:46 2019

@author: mille
"""


import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from PyQt5.Qt import QApplication
import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
from tagazouPlot_ParamDialog_py import paramWidget
from tagazouPlot_SaveWidget_py import saveWidget


def printi():
    print('TODO. Can be done via mainWindow')


class myFigure(Figure):

    def __init__(self, *args, **kwargs):
        Figure.__init__(self, *args, **kwargs)
        self.paramdialog = paramWidget(self)

    def customizeToolbar(self):
        """
        Function that add the save, load and parameters
        button to the toolbar
        """
        self.t = self.canvas.toolbar

        self.t.addAction('Save Data', self.saveWidPop)
        self.t.addSeparator()
        self.t.addAction('Load Data', printi)
        self.t.addSeparator()
        self.t.addAction('Params', self.paramdialog.show)

        # Set size at 8,6 cause it's a cool size
        self.set_size_inches(8, 6)

    def createSaveDataList(self):
        """
        Function that create the mega list string with only the fig data
        that is saved with the saveWidget

        /!\ If you modify it, think on how to modify
        the load figure from the mainwindow
        """
        dataList = ['Figure data\n']
        dataList += ['nbr of plots\t' + str(len(self.axes)) + '\n']
        k = 0
        for ax in self.axes:
            dataList += ['axe' + str(k) + '\n']
            lines = ax.lines
            dataList += ['NumberOfCurve\t' + str(len(lines)) + '\n']
            strcurve = ''

            if len(lines) != 0:
                xdatas = []
                ydatas = []
                i = 0
                for line in lines:
                    xdatas.append(line.get_xdata().tolist())
                    ydatas.append(line.get_ydata().tolist())
                    strcurve += 'curve' + str(i) + '\t\t'
                    i += 1
                strcurve += '\n'
                dataList += [strcurve]
                m = max([len(ydatas[i]) for i in range(len(ydatas))])
                ss = []
                for i in range(m):
                    sss = ''
                    for j in range(len(xdatas)):
                        if i < len(xdatas[j]):
                            sss += str(xdatas[j][i]) + '\t' + str(ydatas[j][i]) + '\t'
                        else:
                            sss += ' \t \t'
                    sss += '\n'
                    ss.append(sss)
                dataList += ss
            k += 1
        return dataList

    def createDataAndStyleList(self):
        """
        Function that create a mega list string with
        the line parameters (linestyle, marker, etc) and the fig data
        that is saved with the savewidget

        /!\ If you modify it, think on how to modify
        the load figure from the mainwindow
        """
        stylelist = ['Styles and General param\n']
        stylelist += ['nbr of plots\t' + str(len(self.axes)) + '\n']
        k = 0
        for ax in self.axes:
            stylelist += ['axe n.\t' + str(k) + '\n']
            s = 'Axe general param\n'
            s += 'titre\t' + ax.get_title() + '\n'
            s += 'xlabel\t' + ax.get_xlabel() + '\n'
            s += 'ylabel\t' + ax.get_ylabel() + '\n'
            s += 'End Axe general param\n'
            stylelist += [s]
            lines = ax.lines
            stylelist += ['NumberOfCurve\t' + str(len(lines)) + '\n']
            if len(lines) != 0:
                i = 0
                for line in lines:
                    stylelist += ['curve n.\t' + str(i) + '\n']
                    # ATTENTION
                    # Starting from here, all the string before the \t
                    # must be valid line param name
                    # for the function set_'paramname'
                    # (c.f the replot function and the function set_line_attr in the main program)
                    stylelist += ['label\t' + str(line.get_label()) + '\n']
                    stylelist += ['linestyle\t' + str(line.get_ls()) + '\n']
                    stylelist += ['linewidth\t' + str(line.get_lw()) + '\n']
                    stylelist += ['marker\t' + str(line.get_marker()) + '\n']
                    stylelist += ['fillstyle\t' + str(line.get_fillstyle()) + '\n']
                    stylelist += ['markeredgecolor\t' + str(line.get_markeredgecolor()) + '\n']
                    stylelist += ['markerfacecolor\t' + str(line.get_markerfacecolor()) + '\n']
                    stylelist += ['markeredgewidth\t' + str(line.get_markeredgewidth()) + '\n']
                    stylelist += ['markersize\t' + str(line.get_markersize()) + '\n']
                    stylelist += ['markevery\t' + str(line.get_markevery()) + '\n']
                    stylelist += ['endstyle' + str(i) + '\n']
                    i += 1
        datalist = self.createSaveDataList()
        savelist = stylelist + ['End Style All\n'] + datalist
        return savelist

    def saveWidPop(self):
        """
        The function save only display the save widget
        """
        self.savewid = saveWidget()
        self.savewid.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)

    fig1 = plt.figure(FigureClass=myFigure)
    fig1.customizeToolbar()
    ax = fig1.add_subplot(111)
    ax.plot([1, 2, 3], [1, 3, 5])
    ax.plot([1.2, 3.5, 4.9, 6.5], [2, 8, 9, 4], '-o')
    plt.show()

    fig2 = plt.figure(fig1.number)

    ll = fig1.createSaveDataList()
    ll2 = fig2.createSaveDataList()

    sys.exit(app.exec_())
