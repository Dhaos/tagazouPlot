# -*- coding: utf-8 -*-
"""
Created on Fri Jan 18 09:48:31 2019

@author: mille
"""

from PyQt5 import QtWidgets, uic
from PyQt5.Qt import QApplication
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import sys
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
from tagazouPlot_CurveWidget_py import curveWidget
from tagazouPlot_SaveWidget_py import saveWidget
from tagazouPlot_myFigure import myFigure

# Load the ui
ui_form_main, ui_base_main = uic.loadUiType(r'tagazouPlot_MainWindow.ui')

def set_line_attr(line, attr, par):
    """
    Function that set a line parameter like linestyle or else
    use to replot a saved figure
    """
    if attr == 'markevery' and par == 'None':
        par = None
    return getattr(line, 'set_'+attr)(par)


class tagazou_ui_main(ui_base_main, ui_form_main):

    curveWis = []
    datas = []
    figs = []
    index = 0
    def __init__(self):
        super(ui_base_main, self).__init__()
        self.setupUi(self)
        self.connectAction()
        self.createCurveWi()
        self.path = ''

    def connectAction(self):
        self.actionLoad_file.triggered.connect(self.loadFile)

        self.actionSave_Fig.triggered.connect(self.saveFig)
        self.actionLoad_Fig_data_only.triggered.connect(self.loadFigDataOnly)
        self.actionLoad_Fig_data_and_style.triggered.connect(self.loadFigDataAndStyle)
        self.actionLoad_Fig_data_style_and_template.triggered.connect(self.loadFigDataStyleAndTemplate)

        self.pushButton_addCurve.clicked.connect(self.createCurveWi)
        self.pushButton_remCurve.clicked.connect(self.removeCurveWi)
        self.pushButton_next.clicked.connect(self.nextWi)
        self.pushButton_prev.clicked.connect(self.prevWi)

        self.pushButton_plot.clicked.connect(self.plotter)
        self.pushButton_update.clicked.connect(self.updateFigList)

    def printi(self):
        print(1)

    def loadFile(self):
        """
        Load a txt file and draw the first 20 lines into the apercu
        Set the file path into class variable self.path
        """
        ftype = "Text files (*.txt)"
        # Ask for a file name, which is return in the first element of the list 'rep'
        rep = QtWidgets.QFileDialog.getOpenFileName(self, "File to plot", filter=ftype)
        # If user cancel, return a ''.
        if rep[0] != '':
            # set the file path into self.path
            self.path = rep[0]
            self.textEdit_apercu.clear()
            file = open(self.path)
            s = file.readlines()
            file.close()
            if len(s) > 20:
                stop = 20
            else:
                stop = len(s)
            ss = ''
            for i in range(stop):
                ss += s[i]
            self.textEdit_apercu.setText(ss)

    def createCurveWi(self):
        """
        Append a curve Widget to the stackedwidget
        that contains the curve widgets
        """
        w = curveWidget(self.stackedWidget, number=len(self.curveWis)+1)
        self.stackedWidget.addWidget(w)
        self.curveWis.append(w)
        self.lcdNumber_curve.display(self.stackedWidget.count())

    def removeCurveWi(self):
        """
        Remove a curve Widget from the stackedwidget
        that contains the curve widgets
        """
        if len(self.curveWis) > 1:
            self.stackedWidget.removeWidget(self.stackedWidget.widget(self.stackedWidget.count()-1))
            self.curveWis = self.curveWis[:-1]
            self.lcdNumber_curve.display(self.stackedWidget.count())

    def nextWi(self):
        """
        Display the next curveWidget
        function called by the arrowed pushbutton next
        """
        if self.index < self.stackedWidget.count()-1:
            self.index += 1
            self.stackedWidget.setCurrentIndex(self.index)
    def prevWi(self):
        """
        Display the previous curveWidget
        function called by the arrowed pushbutton previous
        """
        if self.index >= 1:
            self.index -= 1
            self.stackedWidget.setCurrentIndex(self.index)

    def plotter(self):
        """
        Function that create the figure, one axe into this figure
        and that plot all the curve contained in the stacked widget
        /!\     Use the custom figure class my_figure from tagazouPlot_myFigure.py
                in order to have the plot param and the save figure possibility
        """
        # Use the custom style mpl_simple1 which correpond to rcParams
        # This style is the most beautiful ever done. You should really use it
        # If you don't just comment this line

        if self.comboBox_fig.currentIndex() == 0:
            figu = plt.figure(FigureClass=myFigure)
            figu.customizeToolbar()
        else:
            figNum = int(self.comboBox_fig.currentText().split()[-1])
            figu = plt.figure(figNum, FigureClass=myFigure)
            figu.customizeToolbar()

        titre = self.lineEdit_title.text()
        xlabel = self.lineEdit_x.text()
        ylabel = self.lineEdit_y.text()
        skip = self.spinBox_skip.value()

        for curveWi in self.curveWis:

            leg = curveWi.lineEdit_legend.text()
            colx = curveWi.spinBox_colX.value()
            coly = curveWi.spinBox_colY.value()
            color = curveWi.lineEdit_color.text()

            if colx == -1:
                data = np.loadtxt(self.path, skiprows=skip, usecols=(coly))
                data = data.reshape((len(data), 1))
                a = np.arange(0, len(data), 1).reshape((len(data), 1))
                data = np.concatenate((a, data), axis=1)
            else:
                data = np.loadtxt(self.path, skiprows=skip, usecols=(colx, coly))

            if curveWi.checkBox_err.checkState() == 0:
                errorx = None
                errory = None
            else:
                errory = np.loadtxt(self.path,skiprows=skip, usecols=curveWi.spinBox_coldY.value())
                if curveWi.spinBox_coldX.value() == -1:
                    errorx = None
                else:
                    errorx = np.loadtxt(self.path,skiprows=skip, usecols=curveWi.spinBox_coldX.value())

            if curveWi.lineEdit_xmin.text() != '':
                xmin = float(curveWi.lineEdit_xmin.text())
                data = data[np.where(data[:,0]>=xmin)[0],:]
            if curveWi.lineEdit_xmax.text() != '':
                xmax = float(curveWi.lineEdit_xmax.text())
                data = data[np.where(data[:,0]<=xmax)[0],:]

            # Call the function tagazouPlot just beneath to effectivelly
            # plot the corresponding curve
            ax, figu = self.tagazouPlot(figu, data, leg, color, errorx, errory)

        ax.set_title(titre)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.locator_param(axis='y',prune='lower')
        figu.tight_layout()
        plt.draw()
        plt.show()

    def tagazouPlot(self, figu, data=None, legend='', color='', errorx=None, errory=None):
        """
        Function that do effectivelly plot the data of one curve
        into the 1st axe that is contained into the figure figu
        return :    ax: the axe
                    figu: the figure
        """
        figu.set_size_inches(8,6)
        if len(figu.axes) == 0:
            ax = figu.add_subplot(111)
        else:
            ax = figu.axes[0]

        if errorx == None and errory == None:
            if color == '':
                ax.plot(data[:,0], data[:,1], label=legend)
            else:
                ax.plot(data[:,0], data[:,1], label=legend, color=color)
        else:
            if color == '':
                ax.errorbar(data[:,0], data[:,1], yerr=errory, xerr=errorx, capsize=5, capthick=2, elinewidth=2, label=legend)
            else:
                ax.errorbar(data[:,0], data[:,1], yerr=errory, xerr=errorx, capsize=5, capthick=2, elinewidth=2, label=legend, color=color)

        if legend != '':
            ax.legend().set_draggable(True)

        return ax, figu

    def updateFigList(self):
        """
        Function that update the figure list
        and put them in the List combobox
        Function called by the update pushbutton
        """
        self.figs = plt.get_fignums()
        self.comboBox_fig.clear()
        self.comboBox_fig.addItem('New figure')
        for fignum in self.figs:
            self.comboBox_fig.addItem('Figure '+str(fignum))

    def saveFig(self):
        """
        Function that display the saveWidget in order to save the
        Figure data, style and/or template
        Function called by the Save Fig QAction from the file menu
        """
        self.savewid = saveWidget()
        self.savewid.show()

    def recupPlotParam(self):
        """
        Function that ask for a file name and get the
        Figure data, style and/or template and put all this
        in the datalist which will be used for the replot
        return:     datalist: the datalist
                    rep[0]: the file path
        """
        ftype = "Text files (*.txt)"
        # Ask for a file name, which is return in the first element of the list 'rep'
        rep = QtWidgets.QFileDialog.getOpenFileName(self, "File to recup plot params", filter=ftype)
        datalist = []
        if rep[0] != '':
            file = open(rep[0])
            datalist = file.readlines()
            file.close()
        return datalist, rep[0]

    def plotFigDatafromList(self, datalist, path):
        indexFigdata = datalist.index('Figure data\n')
        nbrofplot = int(datalist[indexFigdata+1].split()[-1])

        fig = plt.figure(figsize=(8, 6), FigureClass=myFigure)
        fig.customizeToolbar()
        for i in range(nbrofplot):
            ax = fig.add_subplot(int(np.ceil(nbrofplot/2.0)), int(np.ceil(nbrofplot/2.0)), i+1)
            axindex = datalist.index('axe'+str(i)+'\n')
            nbrofcurve = int(datalist[axindex+1].split()[-1])
            indexstartdata = axindex+3
            if i+1 == nbrofplot:
                indexstopdata = len(datalist)
            else:
                indexstopdata = datalist.index('axe'+str(i+1)+'\n')
            for j in range(nbrofcurve):
                nnn2 = np.genfromtxt(path, skip_header=indexstartdata,
                                     skip_footer=len(datalist)-indexstopdata,
                                     usecols=(2*j,2*j+1), delimiter='\t')
                x = np.array([nnn2[i,0] for i in range(len(nnn2[:,0])) if ~np.isnan(nnn2[i,0])])
                y = np.array([nnn2[i,1] for i in range(len(nnn2[:,1])) if ~np.isnan(nnn2[i,1])])
                ax.plot(x,y)
        return fig

    def plotFigStylefromList(self, datalist, fig):

        indexFigstyle = datalist.index('Styles and General param\n')
        nbrofplot = int(datalist[indexFigstyle+1].split('\t')[-1])
        if nbrofplot != len(fig.axes):
            print('Not the good nbr of plots for style list')
            return fig

        for i in range(nbrofplot):
            ax = fig.axes[i]
            axindex = datalist.index('axe n.\t'+str(i)+'\n')
            axgenpar1 = datalist[axindex:].index('Axe general param\n')+axindex
            axgenpar2 = datalist[axindex:].index('End Axe general param\n')+axindex
            genpar = datalist[axgenpar1:axgenpar2]

            titre = genpar[1].split('\t')[-1]
            ax.set_title(titre)
            xlabel = genpar[2].split('\t')[-1]
            ax.set_xlabel(xlabel)
            ylabel = genpar[3].split('\t')[-1]
            ax.set_ylabel(ylabel)

            nbrofcurve = int(datalist[axgenpar2+1].split()[-1])
            if nbrofcurve != len(ax.lines):
                print('Not the good nbr of curve for ax n. '+str(i))
                return fig

            for j in range(nbrofcurve):
                line = ax.lines[j]
                curvepar1 = datalist[axindex:].index('curve n.\t'+str(j)+'\n')+axindex
                curvepar2 = datalist[axindex:].index('endstyle'+str(j)+'\n')+axindex
                curvepar = datalist[curvepar1+1:curvepar2]
                for lineattr in curvepar:
                    attr, par = lineattr.split('\t')
                    par = par[:-1]
                    set_line_attr(line,attr,par)
                print('style done for curve '+str(j))
            ax.legend().set_draggable(True)
            print('style done for axe '+str(i))
        print("style done for fig")
        return fig

    def loadFigDataOnly(self):
        datalist, path = self.recupPlotParam()
        fig = self.plotFigDatafromList(datalist, path)
        print('fig data loaded')
        fig.tight_layout()
        # fig.draw()
        fig.show()

    def loadFigDataAndStyle(self):
        datalist, path = self.recupPlotParam()
        fig = self.plotFigDatafromList(datalist, path)
        print('fig data loaded')
        fig = self.plotFigStylefromList(datalist, fig)
        print('fig style loaded')
        fig.tight_layout()
        # fig.draw()
        fig.show()

    def loadFigDataStyleAndTemplate(self):
        datalist, path = self.recupPlotParam()
        fig = self.plotFigDatafromList(datalist, path)
        print('fig data loaded')
        fig = self.plotFigStylefromList(datalist, fig)
        print('fig style loaded')
        fig.paramdialog.setParamsFromList(datalist)
        fig.paramdialog.apply()
        fig.tight_layout()
        # fig.draw()
        fig.show()


if __name__ == '__main__':

    app = QApplication(sys.argv)

    # filepath = os.path.dirname(os.path.realpath(__file__))
    # rcpath = r'\mpl_simple1.mplstyle'
    # plt.style.use(filepath+rcpath)
    matplotlib.use('Qt5Agg')
    plotmain = tagazou_ui_main()
    plotmain.show()
    sys.exit(app.exec_())
